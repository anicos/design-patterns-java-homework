package pl.anicos.exercises.ui.command;

import pl.anicos.exercises.ui.CommandResult;

public interface Command {
    CommandResult execute();

    String getCommandName();

}
