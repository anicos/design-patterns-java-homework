package pl.anicos.exercises.ui.command;

import pl.anicos.exercises.calc.registry.report.DottedReport;
import pl.anicos.exercises.calc.registry.report.Report;
import pl.anicos.exercises.calc.registry.report.ReportFactory;
import pl.anicos.exercises.common.CommandExtractor;
import pl.anicos.exercises.common.ExtractedCommand;
import pl.anicos.exercises.ui.CommandResult;

public class ShowAllCommand implements Command {
    public static final String COMMAND_NAME = "showall";

    private String[] args;

    public ShowAllCommand(String[] args) {
        this.args = args;
    }

    @Override
    public CommandResult execute() {
        ExtractedCommand extractedCommand = (args.length == 0)
                ? new ExtractedCommand(DottedReport.REPORT_NAME, args)
                : CommandExtractor.extract(args);

        Report report = ReportFactory.create(extractedCommand.getName());
        return new CommandResult(report.generateReport());
    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }
}
