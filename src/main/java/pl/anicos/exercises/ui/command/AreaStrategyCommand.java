package pl.anicos.exercises.ui.command;

import pl.anicos.exercises.calc.factory.AreaOperationFactory;
import pl.anicos.exercises.calc.policy.CalculationPolicy;
import pl.anicos.exercises.calc.registry.ReportDbSingleton;
import pl.anicos.exercises.common.Validator;
import pl.anicos.exercises.ui.CommandResult;

public class AreaStrategyCommand implements Command {
    public static final String COMMAND_NAME = "area";
    public static final String SHOULD_HAVE_ARGUMENTS = "Area command should have arguments";
    private final String[] args;

    public AreaStrategyCommand(String[] args) {
        this.args = args;
    }

    @Override
    public CommandResult execute() {
        Validator.isNotEmpty(args, SHOULD_HAVE_ARGUMENTS);
        //strategy pattern
        //----------------
        CalculationPolicy calculationPolicy = AreaOperationFactory.create(args);
        double result = calculationPolicy.calculate();
        //----------------
        CommandResult commandResult = new CommandResult(String.valueOf(result));
        ReportDbSingleton.SINGLETON.addCommand(getCommandName(), commandResult, args);
        return commandResult;
    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }
}
