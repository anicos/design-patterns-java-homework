package pl.anicos.exercises.ui.command;

import pl.anicos.exercises.ui.CommandResult;

public class QuitCommand implements Command {

    public static final String COMMAND_NAME = "quit";

    @Override
    public CommandResult execute() {
        return new CommandResult("Bye bye");
    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }
}
