package pl.anicos.exercises.ui.command;

import pl.anicos.exercises.ui.CommandResult;

public class HelloCommand implements Command {

    public static final String COMMAND_NAME = "hello";

    @Override
    public CommandResult execute() {
        return new CommandResult("Welcome to Area Calculator. Please enter a command");
    }

    @Override
    public String getCommandName() {
        return COMMAND_NAME;
    }

}
