package pl.anicos.exercises.ui.command;

public enum CommandType {
    HELLO("hello"), AREA("area"), QUIT("quit"), SHOW_ALL("showall");

    private final String commandName;

    CommandType(String commandName) {
        this.commandName = commandName;
    }

    public String getCommandName() {
        return commandName;
    }
}
