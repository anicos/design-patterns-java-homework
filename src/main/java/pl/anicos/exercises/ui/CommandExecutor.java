package pl.anicos.exercises.ui;

import pl.anicos.exercises.ui.command.Command;

import java.util.function.Consumer;

public class CommandExecutor {

    private final Consumer<CommandResult> printer;

    public CommandExecutor(Consumer<CommandResult> printer) {
        this.printer = printer;
    }

    public void execute(Command command) {
        CommandResult commandResult = command.execute();
        printer.accept(commandResult);
    }
}
