package pl.anicos.exercises.ui;

public class CommandResult {
    private final String result;

    public CommandResult(String result) {
        this.result = result;
    }

    public String getResult() {
        return result;
    }

    @Override
    public String toString() {
        return result;
    }
}
