package pl.anicos.exercises.ui.factory;

import pl.anicos.exercises.common.CommandExtractor;
import pl.anicos.exercises.common.ExtractedCommand;
import pl.anicos.exercises.ui.command.*;

public class CommandFactory {
    public static Command create(String command) {

        ExtractedCommand extractedCommand = CommandExtractor.extract(command);

        switch (extractedCommand.getName()) {
            case QuitCommand.COMMAND_NAME:
                return new QuitCommand();
            case AreaStrategyCommand.COMMAND_NAME:
                return new AreaStrategyCommand(extractedCommand.getArgs());
            case ShowAllCommand.COMMAND_NAME:
                return new ShowAllCommand(extractedCommand.getArgs());
            case HelloCommand.COMMAND_NAME:
                return new HelloCommand();
        }
        throw new CommandNotFoundException(extractedCommand.getName());
    }
}
