package pl.anicos.exercises.ui.factory;

public class CommandNotFoundException extends IllegalArgumentException {

    public static final String COMMAND_NOT_FOUND = " - command not found";

    public CommandNotFoundException(String commandName) {
        super(commandName + COMMAND_NOT_FOUND);
    }
}
