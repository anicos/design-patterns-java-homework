package pl.anicos.exercises.common;

public class Validator extends IllegalArgumentException {

    public static <T> void hasSize(T[] array, int size, String message) {
        if (array.length != size) {
            throw new IllegalArgumentException(message);
        }
    }

    public static <T> void isNotEmpty(T[] array, String message) {
        if (array.length == 0) {
            throw new IllegalArgumentException(message);
        }
    }

    public static void isDouble(String doubleString, String message) {
        try {
            Double.parseDouble(doubleString);
        } catch (NumberFormatException e) {
            throw new IllegalArgumentException(message);
        }

    }
}
