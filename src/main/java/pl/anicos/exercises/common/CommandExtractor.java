package pl.anicos.exercises.common;

import java.util.Arrays;

public class CommandExtractor {

    public static ExtractedCommand extract(String command) {
        return extract(command.split(" "));
    }

    public static ExtractedCommand extract(String[] args) {
        String commandName = args[0];
        String[] commandArgs = getCommandArgs(args);
        return new ExtractedCommand(commandName, commandArgs);
    }

    private static String[] getCommandArgs(String[] split) {
        return Arrays.copyOfRange(split, 1, split.length);
    }

}



