package pl.anicos.exercises.common;

public class ExtractedCommand {

    private final String name;
    private final String[] args;

    public ExtractedCommand(String name, String[] args) {
        this.name = name;
        this.args = args;
    }

    public String getName() {
        return name;
    }

    public String[] getArgs() {
        return args;
    }
}
