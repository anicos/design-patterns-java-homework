package pl.anicos.exercises.calc.factory;

public class UnknownFigureException extends IllegalArgumentException {

    public UnknownFigureException(String message) {
        super(message);
    }

}
