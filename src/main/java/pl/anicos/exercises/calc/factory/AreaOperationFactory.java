package pl.anicos.exercises.calc.factory;

import pl.anicos.exercises.calc.policy.CalculationPolicy;
import pl.anicos.exercises.calc.policy.RectanglePolicy;
import pl.anicos.exercises.calc.policy.SquarePolicy;
import pl.anicos.exercises.common.CommandExtractor;
import pl.anicos.exercises.common.ExtractedCommand;

public class AreaOperationFactory {

    public static CalculationPolicy create(String[] args) {
        ExtractedCommand extractedCommand = CommandExtractor.extract(args);
        switch (extractedCommand.getName()) {
            case "square":
                return new SquarePolicy(extractedCommand.getArgs());
            case "rectangle":
                return new RectanglePolicy(extractedCommand.getArgs());
        }
        throw new UnknownFigureException("Calculating area for figure '" + extractedCommand.getName() + "' is not implemented");
    }
}
