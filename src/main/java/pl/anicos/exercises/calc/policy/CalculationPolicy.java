package pl.anicos.exercises.calc.policy;

public interface CalculationPolicy {

    double calculate();
}
