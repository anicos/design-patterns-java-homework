package pl.anicos.exercises.calc.policy;

import pl.anicos.exercises.calc.math.RectangleAreaCalculator;
import pl.anicos.exercises.common.Validator;

public class RectanglePolicy implements CalculationPolicy {
    private final String[] args;
    public static final String SHOULD_HAS_EXACTLY = "Area rectangle command should have exactly one argument";
    public static final String SHOULD_BE_DOUBLE = "Area rectangle arguments should be number";

    public RectanglePolicy(String[] args) {
        this.args = args;
        Validator.hasSize(args, 2, SHOULD_HAS_EXACTLY);
        Validator.isDouble(args[0], SHOULD_BE_DOUBLE);
        Validator.isDouble(args[1], SHOULD_BE_DOUBLE);
    }

    @Override
    public double calculate() {
        RectangleAreaCalculator rectangleAreaCalculator = new RectangleAreaCalculator();
        double sideA = Double.parseDouble(args[0]);
        double sideB = Double.parseDouble(args[1]);
        return rectangleAreaCalculator.calculate(sideA, sideB);
    }
}
