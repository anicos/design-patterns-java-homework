package pl.anicos.exercises.calc.policy;

import pl.anicos.exercises.calc.math.RectangleAreaCalculator;
import pl.anicos.exercises.common.Validator;

public class SquarePolicy implements CalculationPolicy {

    public static final String SHOULD_HAS_EXACTLY = "Area square command should have exactly one argument";
    public static final String SHOULD_BE_DOUBLE = "Area square argument should be number";
    private final String[] args;

    public SquarePolicy(String[] args) {
        this.args = args;
        Validator.hasSize(args, 1, SHOULD_HAS_EXACTLY);
        Validator.isDouble(args[0], SHOULD_BE_DOUBLE);
    }

    @Override
    public double calculate() {
        RectangleAreaCalculator rectangleAreaCalculator = new RectangleAreaCalculator();
        double sideSize = Double.parseDouble(args[0]);
        return rectangleAreaCalculator.calculate(sideSize, sideSize);
    }
}
