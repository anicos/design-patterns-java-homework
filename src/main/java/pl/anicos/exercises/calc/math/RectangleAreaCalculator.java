package pl.anicos.exercises.calc.math;

public class RectangleAreaCalculator {

    public double calculate(double sideA, double sideB) {
        return sideA * sideB;
    }
}
