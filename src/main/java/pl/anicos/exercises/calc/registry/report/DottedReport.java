package pl.anicos.exercises.calc.registry.report;

import pl.anicos.exercises.calc.registry.OperationInfo;
import pl.anicos.exercises.calc.registry.ReportDbSingleton;

import java.util.List;

public class DottedReport implements Report {
    public static final String REPORT_NAME = "dotted";
    ReportTemplate reportTemplate = new ReportTemplate();

    @Override
    public String generateReport() {
        List<OperationInfo> operations = ReportDbSingleton.SINGLETON.getOperations();
        return reportTemplate.createReport(operations, "*");
    }
}
