package pl.anicos.exercises.calc.registry;

import java.time.LocalTime;

public class OperationInfo {
    private final LocalTime executionTime;
    private final String command;
    private final String result;

    public OperationInfo(LocalTime executionTime, String command, String result) {
        this.executionTime = executionTime;
        this.command = command;
        this.result = result;
    }

    public LocalTime getExecutionTime() {
        return executionTime;
    }

    public String getCommand() {
        return command;
    }

    public String getResult() {
        return result;
    }
}
