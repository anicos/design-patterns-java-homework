package pl.anicos.exercises.calc.registry.report;

public class ReportFactory {
    public static Report create(String name) {
        switch (name) {
            case DottedReport.REPORT_NAME:
                return new DottedReport();
        }
        throw new UnknownReportType("Can't create report '" + name + "'");
    }
}
