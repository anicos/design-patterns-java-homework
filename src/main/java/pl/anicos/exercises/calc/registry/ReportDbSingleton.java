package pl.anicos.exercises.calc.registry;

import pl.anicos.exercises.ui.CommandResult;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

public enum ReportDbSingleton {
    SINGLETON;

    private final List<OperationInfo> operationInfoList = new ArrayList<>();

    public void addCommand(String commandName, CommandResult commandResult, String... args) {
        String command = commandName + String.join(" ", args);
        OperationInfo operationInfo = new OperationInfoBuilder()
                .setCommand(command)
                .setExecutionTime(LocalTime.now())
                .setResult(commandResult.getResult())
                .build();
        operationInfoList.add(operationInfo);
    }

    public List<OperationInfo> getOperations() {
        return operationInfoList;
    }


}
