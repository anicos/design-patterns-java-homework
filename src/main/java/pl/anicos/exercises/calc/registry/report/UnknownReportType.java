package pl.anicos.exercises.calc.registry.report;

public class UnknownReportType extends IllegalArgumentException {
    public UnknownReportType(String message){
        super(message);
    }
}
