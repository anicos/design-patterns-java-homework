package pl.anicos.exercises.calc.registry;

import java.time.LocalTime;

public class OperationInfoBuilder {
    private LocalTime executionTime;
    private String command;
    private String result;

    public OperationInfoBuilder setExecutionTime(LocalTime executionTime) {
        this.executionTime = executionTime;
        return this;
    }

    public OperationInfoBuilder setCommand(String command) {
        this.command = command;
        return this;
    }

    public OperationInfoBuilder setResult(String result) {
        this.result = result;
        return this;
    }

    public OperationInfo build() {
        return new OperationInfo(executionTime, command, result);
    }
}