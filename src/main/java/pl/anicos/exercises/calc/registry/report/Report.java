package pl.anicos.exercises.calc.registry.report;

public interface Report {
    String generateReport();
}
