package pl.anicos.exercises;

import pl.anicos.exercises.ui.CommandExecutor;
import pl.anicos.exercises.ui.command.Command;
import pl.anicos.exercises.ui.command.HelloCommand;
import pl.anicos.exercises.ui.command.QuitCommand;
import pl.anicos.exercises.ui.factory.CommandFactory;

import java.util.Scanner;
import java.util.function.Consumer;

public class AreaCalculator {

    private final Consumer printer = System.out::println;
    private final CommandExecutor commandExecutor = new CommandExecutor(printer);

    public void run() {
        Command currentCommand = showWelcomeMessage();

        Scanner s = new Scanner(System.in);
        while (!isQuitCommand(currentCommand)) {
            System.out.print("calculator > ");
            String commandString = s.nextLine();
            try {
                currentCommand = CommandFactory.create(commandString);
                commandExecutor.execute(currentCommand);
            } catch (IllegalArgumentException e) {
                printer.accept(e.getMessage());
            }
        }
        s.close();
    }

    private Command showWelcomeMessage() {
        Command currentCommand = CommandFactory.create(HelloCommand.COMMAND_NAME);
        commandExecutor.execute(currentCommand);
        return currentCommand;
    }

    private boolean isQuitCommand(Command currentCommand) {
        return QuitCommand.COMMAND_NAME.equals(currentCommand.getCommandName());
    }
}
