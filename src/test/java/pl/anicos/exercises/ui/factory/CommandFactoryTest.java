package pl.anicos.exercises.ui.factory;

import org.junit.Assert;
import org.junit.Test;
import pl.anicos.exercises.calc.registry.OperationInfo;
import pl.anicos.exercises.calc.registry.ReportDbSingleton;
import pl.anicos.exercises.ui.CommandResult;
import pl.anicos.exercises.ui.command.Command;

import java.util.List;

public class CommandFactoryTest {

    @Test
    public void shouldReturnCorrectResultFromCommand() {
        //given
        Command command = CommandFactory.create("area square 5");
        //when
        CommandResult result = command.execute();
        //then
        Assert.assertEquals("25.0", result.getResult());
    }

    @Test
    public void shouldSaveResultToSingleton() {
        //given
        Command command = CommandFactory.create("area square 5");
        //when
        CommandResult result = command.execute();
        //then
        List<OperationInfo> operations = ReportDbSingleton.SINGLETON.getOperations();
        Assert.assertEquals(1, operations.size());
        Assert.assertEquals("area square 5", operations.get(0).getCommand());
    }
}
